const Sentry = require("@sentry/node");
const https = require("https");

const unsafeHttpsModule = {
  request(options, callback) {
    const allowUnauthorized = process.env.INSECURE_OK == "true";
    console.info(
      "(Making HTTP request with unsafe https module with rejectUnauthorized: %s)",
      !allowUnauthorized
    );
    return https.request(
      { ...options, rejectUnauthorized: !allowUnauthorized },
      callback
    );
  },
};

Sentry.init({
  dsn: "https://glet_85ff0e4488d6c461e11da8fcfcbd9a7d@observe.gitlab.com:443/errortracking/api/v1/projects/43452516",
  debug: true,
  release: "my-javascriprt-project@1.0.0",
  environment: "dev",
  transportOptions: {
    httpModule: unsafeHttpsModule,
  },
});

Sentry.captureException(new Error("Hello from Sentry Javascript SDK"));
